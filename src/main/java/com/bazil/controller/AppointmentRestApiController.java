package com.bazil.controller;


import com.bazil.entity.Appointment;
import com.bazil.service.appointment.AppointmentService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;


@RestController
@RequestMapping("/api")
public class AppointmentRestApiController {
    public static final Logger logger = LoggerFactory.getLogger(AppointmentRestApiController.class);

    @Autowired
    private AppointmentService appointmentService;


    @RequestMapping(value = "/appointment/", method = RequestMethod.GET)
    public ResponseEntity<Page<Appointment>> getAllAppointments(Pageable pageable){
        Page<Appointment> appointments = appointmentService.getAllAppointments(pageable);
        if (appointments.getSize() == 0) {
            return new ResponseEntity(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(appointments, HttpStatus.OK);
    }

    @RequestMapping(value = "/appointment/", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> insertAppointment(@RequestBody Appointment appointment, UriComponentsBuilder ucBuilder){
        logger.info("Creating Appointment : {}", appointment);
        Appointment justCreated = appointmentService.save(appointment);
        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(ucBuilder.path("/api/appointment/{id}").buildAndExpand(appointment.getId()).toUri());
        headers.set("appointmentId",String.valueOf(justCreated.getId()));
        return new ResponseEntity<String>(headers, HttpStatus.CREATED);
    }


    @RequestMapping(value = "/appointment/{id}", method = RequestMethod.GET)
    public ResponseEntity<?> getAppointmentById(@PathVariable("id") long id){
        logger.info("Fetching appointment with id {}", id);
        Appointment appointment = appointmentService.getAppointmentById(id);
        if(appointment == null) {
            logger.error("Appointment with id {} not found", id);
           // return new ResponseEntity(new CustomErrorType("Appointment with id " + id + " not found"), HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(appointment, HttpStatus.OK);
    }

    @RequestMapping(value = "/appointment-by-doctor/{id}", method = RequestMethod.GET)
    public ResponseEntity<Page<Appointment>> getAppointmentByDoctorId(Pageable pageable, @PathVariable("id") long id){
        logger.info("Fetching appointment with id {}", id);
        Page<Appointment> appointments = appointmentService.getAllAppointmentsByDoctorId(pageable,id);
        if (appointments.getSize() == 0) {
            return new ResponseEntity(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(appointments, HttpStatus.OK);
    }

    @RequestMapping(value = "/appointment-by-patient/{id}", method = RequestMethod.GET)
    public ResponseEntity<Page<Appointment>> getAppointmentByPatientId(Pageable pageable, @PathVariable("id") long id){
        logger.info("Fetching appointment with id {}", id);
        Page<Appointment> appointments = appointmentService.getAllAppointmentsByPatientId(pageable, id);
        if (appointments.getSize() == 0) {
            return new ResponseEntity(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(appointments, HttpStatus.OK);
    }

    @RequestMapping(value = "/appointment/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<?> deleteAppointmentById(@PathVariable("id") long id){
        logger.info("Fetching and Deleting Appointment with id {} ", id);
        Appointment appointment = appointmentService.getAppointmentById(id);
        if (appointment == null) {
            logger.error("Fetching and Deleting Appointment with id {} not found ", id);
           // return new ResponseEntity(new CustomErrorType("Unable to delete. Appointment with id " + id + " not found."),
                  //  HttpStatus.NOT_FOUND);
        }
        this.appointmentService.removeAppointmentById(id);
        return new ResponseEntity<Appointment>(HttpStatus.NO_CONTENT);
    }

    @RequestMapping(value = "/appointment/{id}", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Appointment> updateAppointment(@PathVariable("id") int id, @RequestBody Appointment appointment){
        logger.info("Updating appointment wiht id {} ", id);

        Appointment currentAppointment = appointmentService.getAppointmentById(id);

        if(currentAppointment == null) {
            logger.error("Unable to update. Appointment with id {} not found.", id);

           // return new ResponseEntity(new CustomErrorType("Unable to update appointment with id "+ id +"not found"),
             //       HttpStatus.NOT_FOUND);
        }
        currentAppointment.setDoctor(appointment.getDoctor());
        currentAppointment.setPatient(appointment.getPatient());
        currentAppointment.setNotified(appointment.getNotified());
        currentAppointment.setAppointmentDate(appointment.getAppointmentDate());
        currentAppointment.setContent(appointment.getContent());
        currentAppointment.setStatus(appointment.getStatus());
        appointmentService.save(currentAppointment);
        return new ResponseEntity<>(currentAppointment, HttpStatus.OK);
    }

    @RequestMapping(value = "/appointment/", method = RequestMethod.DELETE)
    public ResponseEntity<Appointment> deleteAllUsers() {
        logger.info("Deleting All Appointments");

        appointmentService.deleteAllAppointments();
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

}
