package com.bazil.controller;


import com.bazil.entity.Doctor;
import com.bazil.service.doctor.DoctorService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

@RestController
@RequestMapping("/api")
public class DoctorRestApiController {
    public static final Logger logger = LoggerFactory.getLogger(DoctorRestApiController.class);

    @Autowired
    private DoctorService doctorService;

    /**
     * Retrieve All doctors
     * @return doctors
     */
    @RequestMapping(value = "/doctor/", method = RequestMethod.GET)
    public ResponseEntity<Page<Doctor>> getAllDoctors(Pageable pageable){
        Page<Doctor> doctors = doctorService.getAllDoctors(pageable);
        if (doctors.getSize() == 0) {
            return new ResponseEntity(HttpStatus.NO_CONTENT);
            // or may be return HttpStatus.NOT_FOUND
        }
        return new ResponseEntity<Page<Doctor>>(doctors, HttpStatus.OK);
    }

    /**
     * Store doctor
     * @param doctor
     */
    @RequestMapping(value = "/doctor/", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> insertDoctor(@RequestBody Doctor doctor, UriComponentsBuilder ucBuilder){
        logger.info("Creating Doctor : {}", doctor);

        if (doctorService.isDoctorExist(doctor)) {
            logger.error("Unable to create. A Doctor with name {} already exist", doctor.getFirstname()+" "+doctor.getLastname());
            /*return new ResponseEntity(new CustomErrorType("Unable to create. A Doctor  " +
                    doctor.getFirstname()+" "+doctor.getLastname() + " already exist."), HttpStatus.CONFLICT);*/
        }
        doctorService.storeDoctor(doctor);
        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(ucBuilder.path("/api/doctor/{id}").buildAndExpand(doctor.getId()).toUri());
        headers.set("doctorId",String.valueOf(doctor.getId()));
        return new ResponseEntity<String>(headers, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/doctor/{id}", method = RequestMethod.GET)
    public ResponseEntity<?> getDoctorById(@PathVariable("id") long id){
        logger.info("Fetching doctor with id {}", id);
        Doctor doctor = doctorService.getDoctorById(id);
        if(doctor == null) {
            logger.error("Doctor with id {} not found", id);
          //  return new ResponseEntity(new CustomErrorType("Doctor with id " + id + " not found"), HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<Doctor>(doctor, HttpStatus.OK);
    }

    @RequestMapping(value = "/doctor/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<?> deleteDoctorById(@PathVariable("id") long id){
        logger.info("Fetching and Deleting Doctor with id {} ", id);
        Doctor doctor = doctorService.getDoctorById(id);
        if (doctor == null) {
            logger.error("Fetching and Deleting Doctor with id {} not found ", id);
            /*return new ResponseEntity(new CustomErrorType("Unable to delete. Doctor with id " + id + " not found."),
                    HttpStatus.NOT_FOUND);*/
        }
        this.doctorService.removeDoctorById(id);
        return new ResponseEntity<Doctor>(HttpStatus.NO_CONTENT);
    }


    @RequestMapping(value = "/doctor/{id}", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Doctor> updateDoctor(@PathVariable("id") int id, @RequestBody Doctor doctor){
        logger.info("Updating doctor wiht id {} ", id);

        Doctor currentDoctor = doctorService.getDoctorById(id);

        if(currentDoctor == null) {
            logger.error("Unable to update. Doctor with id {} not found.", id);

            /*return new ResponseEntity(new CustomErrorType("Unable to update doctor with id "+ id +"not found"),
                    HttpStatus.NOT_FOUND);*/
//            return new ResponseEntity<Doctor>(HttpStatus.NOT_FOUND);
        }
        currentDoctor.setFirstname(doctor.getFirstname());
        currentDoctor.setLastname(doctor.getLastname());
        currentDoctor.setEmailAddress(doctor.getEmailAddress());
        currentDoctor.setSpecialization(doctor.getSpecialization());
        doctorService.updateDoctor(currentDoctor);
        return new ResponseEntity<Doctor>(currentDoctor, HttpStatus.OK);
    }


    @RequestMapping(value = "/doctor/", method = RequestMethod.DELETE)
    public ResponseEntity<Doctor> deleteAllUsers() {
        logger.info("Deleting All Doctors");

        doctorService.deleteAllDoctors();
        return new ResponseEntity<Doctor>(HttpStatus.NO_CONTENT);
    }

}
