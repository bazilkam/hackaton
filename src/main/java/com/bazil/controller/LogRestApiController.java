package com.bazil.controller;

import com.bazil.entity.Log;
import com.bazil.service.log.LogService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.Collection;

@RestController
@RequestMapping("/api")
public class LogRestApiController {
    public static final Logger logger = LoggerFactory.getLogger(LogRestApiController.class);

    @Autowired
    private LogService logService;


    /**
     * Retrieve All logs
     * @return logs
     */
    @RequestMapping(value = "/log-by-patient/{id}", method = RequestMethod.GET)
    public ResponseEntity<Collection<Log>> getLogsByPatient(@PathVariable("id") long id){
        Collection<Log> logs = logService.findByPatientId(id);
        if (logs.isEmpty()) {
            return new ResponseEntity(HttpStatus.NO_CONTENT);
            // or may be return HttpStatus.NOT_FOUND
        }
        return new ResponseEntity<Collection<Log>>(logs, HttpStatus.OK);
    }


    /**
     * Retrieve All logs
     * @return logs
     */
    @RequestMapping(value = "/log/", method = RequestMethod.GET)
    public ResponseEntity<Page<Log>> getAllLogs(Pageable pageable){
        Page<Log> logs = logService.getAllLogs(pageable);
        if (logs.getSize() == 0) {
            return new ResponseEntity(HttpStatus.NO_CONTENT);
            // or may be return HttpStatus.NOT_FOUND
        }
        return new ResponseEntity<Page<Log>>(logs, HttpStatus.OK);
    }



    /**
     * Store log
     * JSON:
     *
      	{
		"content": "консультація у лікаря ні для чого",
		"patient": {"id": 3}
	    }
     * @param log
     */
    @RequestMapping(value = "/log/", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> insertLog(@RequestBody Log log, UriComponentsBuilder ucBuilder){
        logger.info("Creating Log : {}", log);
        //todo
//        if (logService.isLogExist(log)) {
//            logger.error("Unable to create. A Log with name {} already exist", log.getFirstname()+" "+log.getLastname());
//            return new ResponseEntity(new CustomErrorType("Unable to create. A Log  " +
//                    log.getFirstname()+" "+log.getLastname() + " already exist."),HttpStatus.CONFLICT);
//        }
        logger.info("log for patient "+log.getPatient(),log.getPatient());
        logService.save(log);
        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(ucBuilder.path("/api/log/{id}").buildAndExpand(log.getId()).toUri());
        headers.set("logId",String.valueOf(log.getId()));
        return new ResponseEntity<String>(headers, HttpStatus.CREATED);
    }


    /**
     * Retrieve Single Log
     * @param id
     * @return
     */
    @RequestMapping(value = "/log/{id}", method = RequestMethod.GET)
    public ResponseEntity<?> getLogById(@PathVariable("id") long id){
        logger.info("Fetching log with id {}", id);
        Log log = logService.getLogById(id);
        if(log == null) {
            logger.error("Log with id {} not found", id);
            //return new ResponseEntity(new CustomErrorType("Log with id " + id + " not found"), HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<Log>(log, HttpStatus.OK);
    }


    /**
     * Delete a log
     * @param id
     */
    @RequestMapping(value = "/log/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<?> deleteLogById(@PathVariable("id") long id){
        logger.info("Fetching and Deleting Log with id {} ", id);
        Log log = logService.getLogById(id);
        if (log == null) {
            logger.error("Fetching and Deleting Log with id {} not found ", id);
         /*   return new ResponseEntity(new CustomErrorType("Unable to delete. Log with id " + id + " not found."),
                    HttpStatus.NOT_FOUND);*/
        }
        this.logService.removeLogById(id);
        return new ResponseEntity<Log>(HttpStatus.NO_CONTENT);
    }

    /**
     * Update a log
     * @param id
     * @param log
     * @return
     */
    @RequestMapping(value = "/log/{id}", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Log> updateLog(@PathVariable("id") int id, @RequestBody Log log){
        logger.info("Updating log wiht id {} ", id);

        Log currentLog = logService.getLogById(id);

        if(currentLog == null) {
            logger.error("Unable to update. Log with id {} not found.", id);

            /*return new ResponseEntity(new CustomErrorType("Unable to update log with id "+ id +"not found"),
                    HttpStatus.NOT_FOUND);*/
//            return new ResponseEntity<Log>(HttpStatus.NOT_FOUND);
        }
        //todo
//        currentLog.setFirstname(log.getFirstname());
//        currentLog.setLastname(log.getLastname());
//        currentLog.setEmailAddress(log.getEmailAddress());
//        currentLog.setSpecialization(log.getSpecialization());
        logService.updateLog(currentLog);
        return new ResponseEntity<Log>(currentLog, HttpStatus.OK);
    }

    /**
     * Delete All logs
     */
    @RequestMapping(value = "/log/", method = RequestMethod.DELETE)
    public ResponseEntity<Log> deleteAllUsers() {
        logger.info("Deleting All Logs");

        logService.deleteAllLogs();
        return new ResponseEntity<Log>(HttpStatus.NO_CONTENT);
    }

}
