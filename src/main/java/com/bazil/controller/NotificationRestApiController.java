package com.bazil.controller;


import com.bazil.entity.Notification;
import com.bazil.service.notification.NotificationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

@RestController
@RequestMapping("/api")
public class NotificationRestApiController {
    public static final Logger logger = LoggerFactory.getLogger(NotificationRestApiController.class);

    @Autowired
    private NotificationService notificationService;

    @RequestMapping(value = "/notification/", method = RequestMethod.GET)
    public ResponseEntity<Page<Notification>> getAllNotifications(Pageable pageable){
        Page<Notification> notifications = notificationService.getAllNotifications(pageable);
        if (notifications.getSize() == 0) {
            return new ResponseEntity(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(notifications, HttpStatus.OK);
    }

    @RequestMapping(value = "/notification/", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> insertNotification(@RequestBody Notification notification, UriComponentsBuilder ucBuilder){
        logger.info("Creating Notification : {}", notification);
        Notification justCreated = notificationService.save(notification);
        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(ucBuilder.path("/api/notification/{id}").buildAndExpand(notification.getId()).toUri());
        headers.set("notificationId",String.valueOf(justCreated.getId()));
        return new ResponseEntity<String>(headers, HttpStatus.CREATED);
    }


    @RequestMapping(value = "/notification/{id}", method = RequestMethod.GET)
    public ResponseEntity<?> getNotificationById(@PathVariable("id") long id){
        logger.info("Fetching notification with id {}", id);
        Notification notification = notificationService.getNotificationById(id);
        if(notification == null) {
            logger.error("Notification with id {} not found", id);
            //return new ResponseEntity(new CustomErrorType("Notification with id " + id + " not found"), HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<Notification>(notification, HttpStatus.OK);
    }

    @RequestMapping(value = "/notification/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<?> deleteNotificationById(@PathVariable("id") long id){
        logger.info("Fetching and Deleting Notification with id {} ", id);
        Notification notification = notificationService.getNotificationById(id);
        if (notification == null) {
            logger.error("Fetching and Deleting Notification with id {} not found ", id);
           /* return new ResponseEntity(new CustomErrorType("Unable to delete. Notification with id " + id + " not found."),
                    HttpStatus.NOT_FOUND);*/
        }
        this.notificationService.removeNotificationById(id);
        return new ResponseEntity<Notification>(HttpStatus.NO_CONTENT);
    }

    @RequestMapping(value = "/notification/{id}", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Notification> updateNotification(@PathVariable("id") int id, @RequestBody Notification notification){
        logger.info("Updating notification wiht id {} ", id);

        Notification currentNotification = notificationService.getNotificationById(id);

        if(currentNotification == null) {
            logger.error("Unable to update. Notification with id {} not found.", id);

           /* return new ResponseEntity(new CustomErrorType("Unable to update notification with id "+ id +"not found"),
                    HttpStatus.NOT_FOUND);*/
        }
        currentNotification.setNotificationStatus(notification.getNotificationStatus());
        currentNotification.setContent(notification.getContent());
        currentNotification.setContentType(notification.getContentType());
        currentNotification.setExpirationDate(notification.getExpirationDate());
        notificationService.save(currentNotification);
        return new ResponseEntity<>(currentNotification, HttpStatus.OK);
    }

    @RequestMapping(value = "/notification/", method = RequestMethod.DELETE)
    public ResponseEntity<Notification> deleteAllUsers() {
        logger.info("Deleting All Notifications");

        notificationService.deleteAllNotifications();
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

}
