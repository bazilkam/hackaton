package com.bazil.controller;

import com.bazil.entity.Patient;
import com.bazil.service.patient.PatientService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

@RestController
@RequestMapping("/api")
public class PatientRestApiController {
    private static final Logger logger = LoggerFactory.getLogger(PatientRestApiController.class);

    @Autowired
    private PatientService patientService;

    public PatientRestApiController() {
    }

    /**
     * Retrieve All patients
     * @return patients
     */
    @RequestMapping(value = "/patient/", method = RequestMethod.GET)
    public ResponseEntity<Page<Patient>> getAllPatients(Pageable pageable){
        Page<Patient> patients = patientService.getAllPatients(pageable);
        if (patients.getSize() == 0) {
            return new ResponseEntity(HttpStatus.NO_CONTENT);
            // or may be return HttpStatus.NOT_FOUND
        }
        return new ResponseEntity<>(patients, HttpStatus.OK);
    }

    /**
     * Store patient
     * @param patient
     */
    @RequestMapping(value = "/patient/", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> insertPatient(@RequestBody Patient patient, UriComponentsBuilder ucBuilder){
        logger.info("Creating Patient : {}", patient);

        if (patientService.isPatientExist(patient)) {
            logger.error("Unable to create. A Patient with name {} already exist", patient.getFirstname()+" "+patient.getLastname());
           /* return new ResponseEntity(new CustomErrorType("Unable to create. A Patient  " +
                    patient.getFirstname()+" "+patient.getLastname() + " already exist."), HttpStatus.CONFLICT);*/
        }
        patientService.storePatient(patient);
        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(ucBuilder.path("/api/patient/{id}").buildAndExpand(patient.getId()).toUri());
        headers.set("patientId",patient.getId().toString());
        return new ResponseEntity<String>(headers, HttpStatus.CREATED);
    }


    @RequestMapping(value = "/patient/{id}", method = RequestMethod.GET)
    public ResponseEntity<?> getPatientById(@PathVariable("id") long id){
        logger.info("Fetching patient with id {}", id);
        Patient patient = patientService.getPatientById(id);
        if(patient == null) {
            logger.error("Patient with id {} not found", id);
           // return new ResponseEntity(new CustomErrorType("Patient with id " + id + " not found"), HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<Patient>(patient, HttpStatus.OK);
    }

    @RequestMapping(value = "/patient/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<?> deletePatientById(@PathVariable("id") long id){
        logger.info("Fetching and Deleting Patient with id {} ", id);
        Patient patient = patientService.getPatientById(id);
        if (patient == null) {
            logger.error("Fetching and Deleting Patient with id {} not found ", id);
            /*return new ResponseEntity(new CustomErrorType("Unable to delete. Patient with id " + id + " not found."),
                    HttpStatus.NOT_FOUND);*/
        }
        this.patientService.removePatientById(id);
        return new ResponseEntity<Patient>(HttpStatus.NO_CONTENT);
    }

    @RequestMapping(value = "/patient/{id}", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Patient> updatePatient(@PathVariable("id") int id, @RequestBody Patient patient){
        logger.info("Updating patient wiht id {} ", id);

        Patient currentPatient = patientService.getPatientById(id);
        if(currentPatient == null) {
            logger.error("Unable to update. Patient with id {} not found.", id);

/*            return new ResponseEntity(new CustomErrorType("Unable to update patient with id "+ id +"not found"),
                    HttpStatus.NOT_FOUND);*/
//            return new ResponseEntity<Patient>(HttpStatus.NOT_FOUND);
        }
        currentPatient.setFirstname(patient.getFirstname());
        currentPatient.setLastname(patient.getLastname());
        currentPatient.setAge(patient.getAge());
        currentPatient.setEmailAddress(patient.getEmailAddress());
        currentPatient.setSex(patient.getSex());
        currentPatient.setPhoneNumber(patient.getPhoneNumber());
        patientService.updatePatient(currentPatient);
        return new ResponseEntity<Patient>(currentPatient, HttpStatus.OK);
    }

    @RequestMapping(value = "/patient/", method = RequestMethod.DELETE)
    public ResponseEntity<Patient> deleteAllUsers() {
        logger.info("Deleting All Patients");
        patientService.deleteAllPatients();
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

}
