package com.bazil.entity;

public enum AppointmentStatus {
    CANCELED, PENDING, HAPPENED;
}
