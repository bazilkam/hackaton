package com.bazil.entity;

import javax.persistence.*;
import java.util.List;

/**
 * Created by Bazil on 11/6/2017.
 */

@Entity
@Table(name = "patientCard")
public class Card {
    @Id
    @GeneratedValue
    private Long id;

    @Column
    private Long patientId;

    @Column
    @OneToMany
    private List<Report> reports;

    public Card() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getPatientId() {
        return patientId;
    }

    public void setPatientId(Long patientId) {
        this.patientId = patientId;
    }

    public List<Report> getReports() {
        return reports;
    }

    public void setReports(List<Report> reports) {
        this.reports = reports;
    }
}
