package com.bazil.entity;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

import static javax.persistence.EnumType.STRING;

@Entity
@Table(name="doctor", uniqueConstraints = {@UniqueConstraint(columnNames = {"firstname","lastname"})})
public class Doctor implements Serializable {
    private static final long serialVersionUID = -3620787914753684394L;

    @Id
    @GeneratedValue
    private Long id;

    @NotEmpty
    @Column
    private String firstname;

    @NotEmpty
    @Column
    private String lastname;

    @NotEmpty
    @Email
    @Column
    private String emailAddress;

    @Column
    @OneToMany(mappedBy = "doctor")
//    @JsonBackReference()
    private List<Appointment> appointments;

    @Column
    @Enumerated(value = STRING)
    private DoctorSpeciality specialization;

    protected Doctor(){}

    public Doctor(Long id, String firstname, String lastname, String emailAddress, DoctorSpeciality specialization) {
        this.id = id;
        this.firstname = firstname;
        this.lastname = lastname;
        this.emailAddress = emailAddress;
        this.specialization = specialization;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public DoctorSpeciality getSpecialization() {
        return specialization;
    }

    public void setSpecialization(DoctorSpeciality specialization) {
        this.specialization = specialization;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Doctor doctor = (Doctor) o;

        if (id != null ? !id.equals(doctor.id) : doctor.id != null) return false;
        if (firstname != null ? !firstname.equals(doctor.firstname) : doctor.firstname != null) return false;
        if (lastname != null ? !lastname.equals(doctor.lastname) : doctor.lastname != null) return false;
        if (emailAddress != null ? !emailAddress.equals(doctor.emailAddress) : doctor.emailAddress != null)
            return false;
        if (appointments != null ? !appointments.equals(doctor.appointments) : doctor.appointments != null)
            return false;
        return specialization == doctor.specialization;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (firstname != null ? firstname.hashCode() : 0);
        result = 31 * result + (lastname != null ? lastname.hashCode() : 0);
        result = 31 * result + (emailAddress != null ? emailAddress.hashCode() : 0);
        result = 31 * result + (appointments != null ? appointments.hashCode() : 0);
        result = 31 * result + (specialization != null ? specialization.hashCode() : 0);
        return result;
    }


    @Override
    public String toString() {
        return "Doctor{" +
                "id=" + id +
                ", firstname='" + firstname + '\'' +
                ", lastname='" + lastname + '\'' +
                ", emailAddress='" + emailAddress + '\'' +
                ", specialization=" + specialization +
                '}';
    }
}
