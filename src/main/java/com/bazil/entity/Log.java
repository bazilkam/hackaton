package com.bazil.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

@Entity
public class Log implements Serializable {
    private static final long serialVersionUID = 7689724387254248676L;
    @Id
    @GeneratedValue
    private Long id;
    @Column
    private String content;

    @Column
    private LocalDateTime dateTime;


    @ManyToOne(optional = false)
    @JoinColumn(name = "patient", referencedColumnName = "id")
    private Patient patient;

    public void setId(Long id) {
        this.id = id;
    }

    @Column
    public LocalDateTime getDateTime() {
        return dateTime;
    }

    public void setDateTime(LocalDateTime dateTime) {
        this.dateTime = dateTime;
    }

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    protected Log() {
    }

    public Log(String content, Patient patient) {
        this.content = content;
        this.patient = patient;
    }

    public Long getId() {
        return id;
    }


    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Override
    public String toString() {
        return "Log{" +
                "id=" + id +
                ", content='" + content + '\'' +
                ", dateTime=" + dateTime +
                ", patient=" + patient +
                '}';
    }
}
