package com.bazil.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Collection;

@Entity
public class Notification implements Serializable{

    private static final long serialVersionUID = -6159819336447977570L;
    @Id
    @GeneratedValue
    private Long id;

    @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinTable(name = "notification_patient",
            joinColumns = {@JoinColumn(name = "notification_id")},
            inverseJoinColumns = {@JoinColumn(name = "patient_id")})
    private Collection<Patient> patients;

    //todo: не бачу сенсу в цьому полі
    @Column
    @Enumerated(EnumType.STRING)
    private ContentType contentType;

    @Column
    private String content;

    @Column
    @Enumerated(EnumType.STRING)
    private NotificationStatus notificationStatus;

    @Column
    private LocalDateTime expirationDate;

    protected Notification(){}

    public Notification(Long id, Collection<Patient> patients, ContentType contentType, String content, NotificationStatus notificationStatus, LocalDateTime expirationDate) {
        this.id = id;
        this.patients = patients;
        this.contentType = contentType;
        this.content = content;
        this.notificationStatus = notificationStatus;
        this.expirationDate = expirationDate;
    }

    public Collection<Patient> getPatients() {
        return patients;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }


    public ContentType getContentType() {
        return contentType;
    }

    public void setContentType(ContentType contentType) {
        this.contentType = contentType;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public NotificationStatus getNotificationStatus() {
        return notificationStatus;
    }

    public void setNotificationStatus(NotificationStatus notificationStatus) {
        this.notificationStatus = notificationStatus;
    }

    public LocalDateTime getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(LocalDateTime expirationDate) {
        this.expirationDate = expirationDate;
    }

    @Override
    public String toString() {
        return "Notification{" +
                "id=" + id +
                ", patients=" + patients +
                ", contentType=" + contentType +
                ", content='" + content + '\'' +
                ", notificationStatus=" + notificationStatus +
                ", expirationDate=" + expirationDate +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Notification)) return false;

        Notification that = (Notification) o;

        if (getId() != that.getId()) return false;
        if (!getPatients().equals(that.getPatients())) return false;
        if (getContentType() != that.getContentType()) return false;
        if (getContent() != null ? !getContent().equals(that.getContent()) : that.getContent() != null) return false;
        if (getNotificationStatus() != that.getNotificationStatus()) return false;
        return getExpirationDate().equals(that.getExpirationDate());
    }

    @Override
    public int hashCode() {
        int result = (int) (getId() ^ (getId() >>> 32));
        result = 31 * result + getPatients().hashCode();
        result = 31 * result + (getContentType() != null ? getContentType().hashCode() : 0);
        result = 31 * result + (getContent() != null ? getContent().hashCode() : 0);
        result = 31 * result + (getNotificationStatus() != null ? getNotificationStatus().hashCode() : 0);
        result = 31 * result + getExpirationDate().hashCode();
        return result;
    }
}
