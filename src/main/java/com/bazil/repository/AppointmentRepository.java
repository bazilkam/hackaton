package com.bazil.repository;

import com.bazil.entity.Appointment;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AppointmentRepository extends PagingAndSortingRepository<Appointment, Long> {
    Appointment findById(long id);

    List<Appointment> findAll();

    Page<Appointment> findAll(Pageable pageable);

    List<Appointment> findByDoctorId(long id);

    Page<Appointment> findByDoctorId(Pageable pageable, long id);

    List<Appointment> findByPatientId(long id);

    Page<Appointment> findByPatientId(Pageable pageable, long id);

    void deleteAll();

    void delete(long id);

    Appointment findOne(long id);

    Appointment save(Appointment appointment);
}
