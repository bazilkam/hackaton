package com.bazil.repository;

import com.bazil.entity.Card;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by Bazil on 11/11/2017.
 */
public interface CardRepository extends JpaRepository<Card, Long> {
}
