package com.bazil.repository;

import com.bazil.entity.Doctor;
import com.bazil.entity.DoctorSpeciality;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DoctorRepository extends PagingAndSortingRepository<Doctor, Long> {
    Doctor findByLastnameAndFirstname(String lastname, String firstname);

    Page<Doctor> findBySpecialization(Pageable pageable, DoctorSpeciality doctorSpeciality);

    void findById(long id);

    Page<Doctor> findByEmailAddress(Pageable pageable, String eMailAddress);

    List<Doctor> findAll();

    Page<Doctor> findAll(Pageable pageable);

    void delete(long id);
}
