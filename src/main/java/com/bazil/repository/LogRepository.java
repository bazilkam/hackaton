package com.bazil.repository;

import com.bazil.entity.Log;
import com.bazil.entity.Patient;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface LogRepository extends PagingAndSortingRepository<Log, Long> {
    Log findById(long id);

    List<Log> findAll();

    Page<Log> findAll(Pageable pageable);

    void deleteAll();

    void delete(long id);

    Log findOne(long id);

    Log save(Log log);

    List<Log> findByPatient(Patient patient);
}
