package com.bazil.repository;

import com.bazil.entity.Notification;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface NotificationRepository extends PagingAndSortingRepository<Notification, Long> {
    Notification findById(Long id);

    List<Notification> findAll();

    Page<Notification> findAll(Pageable pageable);

    void deleteAll();

    void delete(Long id);

    Notification findOne(Long id);

    Notification save(Notification notification);
}
