package com.bazil.repository;

import com.bazil.entity.Patient;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PatientRepository extends PagingAndSortingRepository<Patient, Long> {
    List<Patient> findAll();

    Page<Patient> findAll(Pageable pageable);

    List<Patient> findByFirstname(String firstname);

    Page<Patient> findByFirstname(String firstname, Pageable pageable);

    Patient findByLastnameAndFirstname(String lastname, String firstname);

    Patient findById(long id);

    Patient findByEmailAddress(String emailAddress);

    List<Patient> findByLastname(String lastname);

    Page<Patient> findByLastname(String lastname, Pageable pageable);

}
