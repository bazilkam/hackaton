package com.bazil.service.appointment;


import com.bazil.entity.Appointment;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

/**
 * Created by andriy on 31.07.17.
 */
public interface AppointmentService {
    List<Appointment> getAllAppointments();
    Page<Appointment> getAllAppointments(Pageable pageable);
    List<Appointment> getAllAppointmentsByDoctorId(long id);
    Page<Appointment> getAllAppointmentsByDoctorId(Pageable pageable, long id);
    List<Appointment> getAllAppointmentsByPatientId(long id);
    Page<Appointment> getAllAppointmentsByPatientId(Pageable pageable, long id);
    Appointment save(Appointment appointment);
    Appointment update(Appointment appointment);
    Appointment getAppointmentById(long id);
    void removeAppointmentById(long id);
    boolean isAppointmentExist(Appointment appointment);
    void deleteAllAppointments();
}
