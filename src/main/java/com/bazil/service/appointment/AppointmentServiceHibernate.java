package com.bazil.service.appointment;

import com.bazil.entity.Appointment;
import com.bazil.repository.AppointmentRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AppointmentServiceHibernate implements AppointmentService {
    private static final Logger logger = LoggerFactory.getLogger(AppointmentServiceHibernate.class);

    @Autowired
    private AppointmentRepository appointmentRepository;

    public AppointmentServiceHibernate(AppointmentRepository appointmentRepository) {
        this.appointmentRepository = appointmentRepository;
    }

    public AppointmentServiceHibernate() {
    }

    @Override
    public List<Appointment> getAllAppointments() {
        return appointmentRepository.findAll();
    }

    @Override
    public Page<Appointment> getAllAppointments(Pageable pageable) {
        return appointmentRepository.findAll(pageable);
    }

    @Override
    public List<Appointment> getAllAppointmentsByDoctorId(long id) {
        return appointmentRepository.findByDoctorId(id);
    }

    @Override
    public Page<Appointment> getAllAppointmentsByDoctorId(Pageable pageable, long id) {
        return appointmentRepository.findByDoctorId(pageable, id);
    }

    @Override
    public List<Appointment> getAllAppointmentsByPatientId(long id) {
        return appointmentRepository.findByPatientId(id);
    }

    @Override
    public Page<Appointment> getAllAppointmentsByPatientId(Pageable pageable, long id) {
        return appointmentRepository.findByPatientId(pageable, id);
    }

    public Appointment save(Appointment appointment) {
        appointmentRepository.save(appointment);
        return appointment;
    }

    @Override
    public Appointment update(Appointment appointment) {
        logger.info("appointment update ", appointment);
        long id = appointment.getId();
        Appointment updatedAppointment = appointmentRepository.findOne(id);
        updatedAppointment = appointment;
        appointmentRepository.save(updatedAppointment);
        return updatedAppointment;

    }

    @Override
    public Appointment getAppointmentById(long id) {
        return appointmentRepository.findOne(id);
    }

    @Override
    public void removeAppointmentById(long id) {
        appointmentRepository.delete(id);
    }

    @Override
    public boolean isAppointmentExist(Appointment appointment) {
        return appointmentRepository.exists(appointment.getId());
    }

    @Override
    public void deleteAllAppointments() {
        appointmentRepository.deleteAll();
    }

}
