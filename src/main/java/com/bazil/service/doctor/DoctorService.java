package com.bazil.service.doctor;

import com.bazil.entity.Doctor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface DoctorService {
    Page<Doctor> getAllDoctors(Pageable pageable);

    Doctor getDoctorByFirstnameAndLastname(String firstname, String lastname);

    void storeDoctor(Doctor doctor);

    void updateDoctor(Doctor doctor);

    Doctor getDoctorById(long id);

    void removeDoctorById(long id);

    boolean isDoctorExist(Doctor doctor);

    void deleteAllDoctors();
}
