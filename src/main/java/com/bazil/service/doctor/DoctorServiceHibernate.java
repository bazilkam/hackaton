package com.bazil.service.doctor;

import com.bazil.entity.Doctor;
import com.bazil.repository.DoctorRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class DoctorServiceHibernate implements DoctorService {
    private static final Logger logger = LoggerFactory.getLogger(DoctorServiceHibernate.class);

    @Autowired
    private DoctorRepository doctorRepository;

    public DoctorServiceHibernate(DoctorRepository doctorRepository) {
        this.doctorRepository = doctorRepository;
    }

    @Override
    public Page<Doctor> getAllDoctors(Pageable pageable) {
        return doctorRepository.findAll(pageable);
    }

    @Override
    public Doctor getDoctorByFirstnameAndLastname(String firstname, String lastname) {
        return doctorRepository.findByLastnameAndFirstname(lastname, firstname);
    }

    public void storeDoctor(Doctor doctor) {
        doctorRepository.save(doctor);
    }

    @Override
    public void updateDoctor(Doctor doctor) {
        logger.info("doctor update ", doctor);
        long id = doctor.getId();
        Doctor updatedDoctor = doctorRepository.findOne(id);
        updatedDoctor = doctor;
        doctorRepository.save(updatedDoctor);
    }

    @Override
    public Doctor getDoctorById(long id) {
        return doctorRepository.findOne(id);
    }

    @Override
    public void removeDoctorById(long id) {
        doctorRepository.delete(id);
    }

    @Override
    public boolean isDoctorExist(Doctor doctor) {
        Doctor founddoctor = doctorRepository.findByLastnameAndFirstname(doctor.getLastname(), doctor.getFirstname());
        return founddoctor != null;
    }

    @Override
    public void deleteAllDoctors() {
        doctorRepository.deleteAll();
    }

}
