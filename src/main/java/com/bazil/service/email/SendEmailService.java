package com.bazil.service.email;


import com.bazil.entity.Notification;

public interface SendEmailService {
    void sendEmail(Notification notification);
}
