package com.bazil.service.email;

import com.bazil.entity.Notification;
import com.bazil.entity.Patient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;


import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Properties;

@Service
@Configuration
@PropertySource("file:emailserver.properties")
public class SendEmailServiceTLSGmail implements SendEmailService {
    private static final Logger LOGGER = LoggerFactory.getLogger(SendEmailServiceTLSGmail.class);

    @Autowired
    Environment environment;

    protected SendEmailServiceTLSGmail(){}

    public void sendEmail(String...args) {
        String username = environment.getProperty("emailserver.username");
        String password = environment.getProperty("emailserver.password");


        Properties props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "587");

        Session session = Session.getInstance(props,
                new javax.mail.Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(username, password);
                    }
                });

        try {

            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress("from-email@gmail.com"));
            message.setRecipients(Message.RecipientType.TO,
                    InternetAddress.parse("l.andriy@gmail.com"));
            message.setSubject("Clinic: Visit your doctor at " + LocalDateTime.now()); // subject
            message.setText("Dear Andriy ,"
                    + "\n\n You have to visit your doctor at " + LocalDateTime.now()
                    + "\n\n"
            );

            Transport.send(message);

            System.out.println("Done");

        } catch (MessagingException e) {
            LOGGER.warn("Progblem with sending e-mail",e);
        }
    }
    @Override
    public void sendEmail(Notification notification) {
        String username = environment.getProperty("emailserver.username");
        String password = environment.getProperty("emailserver.password");
        Patient patient = ((List<Patient>)notification.getPatients()).get(0);

        Properties props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "587");

        Session session = Session.getInstance(props,
                new javax.mail.Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(username, password);
                    }
                });

        try {

            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress("clinic@unicorn-hospital.com"));
            message.setRecipients(Message.RecipientType.TO,
                    InternetAddress.parse(patient.getEmailAddress()));
            message.setSubject("Clinic: Visit your doctor at " + notification.getExpirationDate()); // subject
            message.setText(notification.getContent() );

            Transport.send(message);

            System.out.println("Done");

        } catch (MessagingException e) {
            LOGGER.warn("Progblem with sending e-mail",e);
        }
    }

}
