package com.bazil.service.log;

import com.bazil.entity.Log;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface LogService {
    Log save(Log log);

    List<Log> getAllLogs();

    Page<Log> getAllLogs(Pageable pageable);

    void updateLog(Log log);

    Log getLogById(long id);

    void removeLogById(long id);

    boolean isLogExist(Log log);

    void deleteAllLogs();

    List<Log> findByPatientId(Long id);

}
