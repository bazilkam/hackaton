package com.bazil.service.log;

import com.bazil.entity.Log;
import com.bazil.repository.LogRepository;
import com.bazil.repository.PatientRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.List;

@Service("logServiceHibernate")
public class LogServiceHibernate implements LogService{
    private static final Logger logger = LoggerFactory.getLogger(LogServiceHibernate.class);

    @Autowired
    private LogRepository logRepository;

    @Autowired
    private PatientRepository patientRepository;

    @PersistenceContext
    private EntityManager em;

    public LogServiceHibernate(LogRepository logRepository) {
        this.logRepository = logRepository;
    }

    @Override
    public Log save(Log log) {
        if(log.getId() == null) {
            log.setDateTime(LocalDateTime.now(ZoneId.of("Europe/Kiev")));
            logRepository.save(log);
            return log;
        } else {
            return em.merge(log);
        }
    }

    @Override
    public List<Log> findByPatientId (Long id) {
        TypedQuery query = em.createQuery("select l from Log l where l.patient = ?1", Log.class);
        query.setParameter(1, patientRepository.findById(id));
        return  query.getResultList();
    }

    @Override
    public List<Log> getAllLogs() {
        return logRepository.findAll();
    }

    @Override
    public Page<Log> getAllLogs(Pageable pageable) {
        return logRepository.findAll(pageable);
    }


    @Override
    public void updateLog(Log log) {
        logger.info("log update ", log);
        long id = log.getId();
        Log updatedLog = logRepository.findOne(id);
        updatedLog = log;
        logRepository.save(updatedLog);
    }

    @Override
    public Log getLogById(long id) {
        return logRepository.findOne(id);
    }

    @Override
    public void removeLogById(long id) {
        logRepository.delete(id);
    }

    @Override
    public boolean isLogExist(Log log) {
        return logRepository.exists(log.getId());
    }

    @Override
    public void deleteAllLogs() {
        logRepository.deleteAll();
    }


}
