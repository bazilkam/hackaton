package com.bazil.service.notification;

import com.bazil.entity.Notification;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Created by andriy on 31.07.17.
 */
public interface NotificationService {
    Page<Notification> getAllNotifications(Pageable pageable);
    Notification save(Notification notification);
    Notification update(Notification notification);
    Notification getNotificationById(long id);
    void removeNotificationById(long id);
    boolean isNotificationExist(Notification notification);
    void deleteAllNotifications();
}
