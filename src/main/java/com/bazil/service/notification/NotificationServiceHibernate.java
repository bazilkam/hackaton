package com.bazil.service.notification;

import com.bazil.entity.Notification;
import com.bazil.repository.NotificationRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class NotificationServiceHibernate implements NotificationService{
    private static final Logger logger = LoggerFactory.getLogger(NotificationServiceHibernate.class);

    @Autowired
    private NotificationRepository notificationRepository;

    public NotificationServiceHibernate(NotificationRepository notificationRepository) {
        this.notificationRepository = notificationRepository;
    }

    public NotificationServiceHibernate() {
    }

    @Override
    public Page<Notification> getAllNotifications(Pageable pageable) {
        return notificationRepository.findAll(pageable);
    }

    public Notification save(Notification notification) {
        notificationRepository.save(notification);
        return notification;
    }

    @Override
    public Notification update(Notification notification) {
        logger.info("notification update ", notification);
        long id = notification.getId();
        Notification updatedNotification = notificationRepository.findOne(id);
        updatedNotification = notification;
        notificationRepository.save(updatedNotification);
        return updatedNotification;

    }

    @Override
    public Notification getNotificationById(long id) {
        return notificationRepository.findOne(id);
    }

    @Override
    public void removeNotificationById(long id) {
        notificationRepository.delete(id);
    }

    @Override
    public boolean isNotificationExist(Notification notification) {
        return notificationRepository.exists(notification.getId());
    }

    @Override
    public void deleteAllNotifications() {
        notificationRepository.deleteAll();
    }

}
