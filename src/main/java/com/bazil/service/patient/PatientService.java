package com.bazil.service.patient;

import com.bazil.entity.Patient;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface PatientService {
    List<Patient> getAllPatients();

    Page<Patient> getAllPatients(Pageable pageable);

    void storePatient(Patient patient);

    void updatePatient(Patient patient);

    Patient getPatientById(long id);

    void removePatientById(long id);

    boolean isPatientExist(Patient patient);

    void deleteAllPatients();
}
