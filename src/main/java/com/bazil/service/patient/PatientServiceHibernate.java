package com.bazil.service.patient;

import com.bazil.entity.Patient;
import com.bazil.repository.PatientRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PatientServiceHibernate implements PatientService {
    private static final Logger logger = LoggerFactory.getLogger(PatientServiceHibernate.class);

    @Autowired
    private PatientRepository patientRepository;

    public PatientServiceHibernate(PatientRepository patientRepository) {
        this.patientRepository = patientRepository;
    }

    @Override
    public List<Patient> getAllPatients() {
        return patientRepository.findAll();
    }

    @Override
    public Page<Patient> getAllPatients(Pageable pageable) {
        return patientRepository.findAll(pageable);
    }

    public void storePatient(Patient patient) {
        patientRepository.save(patient);
    }

    @Override
    public void updatePatient(Patient patient) {
        logger.info("patient hib {}", patient);
        long id = patient.getId();
        Patient updatedPatient = patientRepository.findOne(id);
        updatedPatient.setFirstname(patient.getFirstname());
        updatedPatient.setLastname(patient.getLastname());
        updatedPatient.setAge(patient.getAge());
        updatedPatient.setEmailAddress(patient.getEmailAddress());
        updatedPatient.setPhoneNumber(patient.getPhoneNumber());
        updatedPatient.setSex(patient.getSex());
        patientRepository.save(updatedPatient);
    }

    @Override
    public Patient getPatientById(long id) {
        return patientRepository.findOne(id);
    }

    @Override
    public void removePatientById(long id) {
        if (patientRepository.findOne(id) != null) {
            patientRepository.delete(id);
        }
    }

    @Override
    public boolean isPatientExist(Patient patient) {
        Patient foundPatient = patientRepository.findByLastnameAndFirstname(patient.getLastname(), patient.getFirstname());
        return foundPatient != null;
    }

    @Override
    public void deleteAllPatients() {
        patientRepository.deleteAll();
    }

}
